## "baseUrl": "./src"

In the project directory, you can run:

### `yarn start` ### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## มาตรฐานในการตั้งชื่อตัวแปร

Pascal Case ==> เป็นกฏการตั้งชื่อที่กำหนดให้ตัวอักษรแรกของแต่ละคำเป็นตัวใหญ่ เช่น ReactTemplateFeatureBased
Camel Case ==> โดยที่ตัวอักษรแรกของคำจะเป็นตัวพิมพ์เล็ก เช่น reactTemplateFeatureBased
snake case ==> การแทนการเว้นวรรคด้วยเครื่องหมาย Underscore (_) โดยสามารถใช้ร่วมกับการเขียนรูปแบบอื่น ๆ ได้ เช่น react_template_feature_based

**ว่าด้วยการตั้งชื่อไฟล์และโฟลเดอร์ ควรตั้งชื่อด้วยตัวเล็กหมดและกำหนดให้คำถัดไปมี "-" คั่นข้างหน้า เช่น react-template-feature-based

**การตั้งชื่อ Class component, function component, Higher-Order Component (HOC) ควรเป็น Pascal Case เช่น class HelloComponent, function HelloComponent(), const NewHOC = (SomeComponent) => {} 

**การตั้งชื่อ method ควรเป็น Camel Case เช่น componentDidMount(), getInfo(), getApi() หรืออื่นๆ

**การตั้งชื่อตัวแปรต่างๆ ควรเป็น snake case เช่น react_template_feature_based

****ทั้งหมดนี้เพื่อให้ง่ายต่อการจำแนก ไฟล์, โฟลเดอร์, Class component, function component, method, ตัวแปร 

## Style ## การตั้งชื่อคลาสใน style

## การตั้งชื่อแบบ BEM (ไม่ใช้เสียเวลา)
.home__banner__button
.home__banner__button--red 
.home__banner__button--is-active
***เวลาเราใช้ BEM ในระดับที่ลึก ๆ หลายชั้น บางคนจะเขียน Selector จนกลายเป็นชื่อ Class ยาวมาก ๆ ดูไม่งาม ไฟล์ HTML จะโค้ดยาว พิมพ์นาน เสียเวลาอีกด้วย

## การตั้งชื่อแบบ RSCSS (ใช้อันนี้)
RSCSS แบ่ง Component ออกเป็น Element
ในการเขียน CSS Selector ของ Element ควรเขียนในรูปแบบ .component > .element เพื่อป้องกัน CSS ไปกระทบกับ Element ชื่อเดียวคลาสกันที่ซ้อนอยู่ด้านในอีกที
จากตัวอย่างข้างบน โค้ดก็จะออกมาเป็น
.news-form > .title
.news-form > .input
.news-form > .button

หรือถ้าใช้ SASS

.news-form {
  > .title 
  > .input 
  > .button 
}

## การใช้ RSCSS สำหรับคลาส Utility, Helper
คลาส Utility หรือ Helper เป็นคลาสพิเศษที่ใช้เพื่อช่วยตกแต่งเล็ก ๆ น้อย ๆ เพิ่มเติม และสามารถใช้ได้กับ Component / Element ไหนก็ได้ เช่น คลาส .text-right สำหรับจัดตัวหนังสือให้อยู่ด้านขวา หรือ .center-block สำหรับจัดกล่องให้อยู่ตรงกลาง เป็นต้น
**ใน RSCSS นั้น Helper Class จะใช้วิธีตั้งชื่อแบบ Underscore (_) ตามด้วยชื่อคลาส เช่น ._nomargin, ._center, ._text-center

**สาเหตุที่ใช้ Underscore แทน เพราะจะทำให้เวลาเขียนใน HTML เรามองเห็นชัดเจนระหว่างคลาสไหนเป็น Element และคลาสไหนเป็น Utility 

## ตัวอย่าง
.news-form {
  > .-slim{
      &._unmargin{

      }
  } 
}

<div class='news-form'>
    <div class="-slim _unmargin"></div>
</div>

จะเห็นว่าการเขียนแบบ RSCSS ใน HTML ชื่อคลาสแต่ละส่วนจะให้ความชัดเจนว่าส่วนไหนเป็นอะไรเหมือน BEM โดยที่โค้ดสั้นลงกว่าเยอะ เพราะไม่ต้องมาเขียนชื่อคลาสหลักซ้ำหลาย ๆ รอบ และใช้ 1 ขีดแทน 2 ขีด

## static package.json
node-sass, react-bootstrap, react-router-dom

## import bootstrap จาก CDN ดีกว่า npm install
